package org.remote

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActors, TestKit}
import com.typesafe.config.ConfigFactory
import org.remote.controller.ControllerActor
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class ControllerActorSpec() extends TestKit(ActorSystem("TestSystem", ConfigFactory.load("testing.conf")))
  with ImplicitSender with WordSpecLike with Matchers with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "An Echo actor" must {
    "send back messages unchanged" in {
      val host = "testhost"
      val actor = system.actorOf(ControllerActor.props(host), s"${host}controller")
      actor ! "hello world"
      expectMsg("hello world")
    }
  }
}

