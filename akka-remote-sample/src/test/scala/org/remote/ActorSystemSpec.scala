package org.remote

import akka.actor.ActorSystem
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit}
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class ActorSystemSpec(system: ActorSystem) extends TestKit(system)
  with DefaultTimeout with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  override def afterAll: Unit = {
    //    TestKit.shutdownActorSystem()
  }

  def this() = this(ActorSystem("TestSystem", ConfigFactory.load("testing.conf")))
}

