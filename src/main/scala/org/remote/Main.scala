package org.remote

import org.remote.utils.CLIInputProcessor._
import org.remote.utils._

import scala.util.Failure

object Main extends App {

  val tryConf = CLIInputProcessor.configuration(args)
  for (conf <- tryConf) conf match {
    case config: ControllerConfig =>
      println("Starting controller role")
      Role.startController(config)
    case config: SenderConfig =>
      println("Starting sender role")
      Role.startSender(config)
    case other: Any =>
      println(s"Something was wrong $other")
  }

  tryConf match {
    case Failure(ex) => ex.printStackTrace()
    case _ =>

  }
}